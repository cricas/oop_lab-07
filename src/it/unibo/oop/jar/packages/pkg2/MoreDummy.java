/**
 * 
 */
package it.unibo.oop.jar.packages.pkg2;

import it.unibo.oop.jar.packages.pkg1.DummyClass;

/**
 * Dummy class, just to try by-hand compilation.
 * 
 */
public class MoreDummy extends DummyClass {

    @Override
    public String toString() {
        return "More";
    }

}
